package com.github.mzule.abilityrouter.router;

import ohos.aafwk.content.IntentParams;
import ohos.app.Context;
/**
 * Created by CaoDongping on 04/11/2016.
 */
public interface MethodInvoker {
    /**
     * 调用
     *
     *  @param context 上下文
     *  @param bundle 存取内容
     */
    void invoke(Context context, IntentParams bundle);
}
