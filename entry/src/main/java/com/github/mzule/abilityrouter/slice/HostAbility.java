package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.annotation.Router;

/**
 * @author Kale
 * @date 2016/8/9
 */
@Router("with_host")
public class HostAbility extends DumpExtrasAbility {

}