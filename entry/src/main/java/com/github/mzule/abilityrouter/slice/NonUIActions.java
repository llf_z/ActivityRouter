package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.annotation.Router;
import ohos.aafwk.content.IntentParams;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
/**
 * Created by CaoDongping on 04/11/2016.
 */
public class NonUIActions {
    /**
     * 定义一个登出的方法
     *
     * @param context 上下文
     * @param bundle 接收的值
     */
    @Router("logout")
    public static void logout(Context context, IntentParams bundle) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setText("logout").show();
    }
    /**
     * 定义一个上传的方法
     *
     * @param context 上下文
     * @param bundle 接收的值
     */
    @Router("upload")
    public static void uploadLog(Context context, IntentParams bundle) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setText("upload").show();
    }
}
