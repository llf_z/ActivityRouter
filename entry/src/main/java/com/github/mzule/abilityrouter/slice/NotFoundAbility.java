package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.utils.TextAlignment;
/**
 * Created by CaoDongping on 4/8/16.
 */
public class NotFoundAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Text componentById = (Text) findComponentById(ResourceTable.Id_text_helloworld);
        componentById.setText("404");
        componentById.setTextAlignment(TextAlignment.CENTER);
    }
}
