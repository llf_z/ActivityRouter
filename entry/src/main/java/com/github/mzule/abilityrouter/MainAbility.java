package com.github.mzule.abilityrouter;

import com.github.mzule.abilityrouter.annotation.Router;
import com.github.mzule.abilityrouter.slice.DumpExtrasAbility;
import ohos.aafwk.content.Intent;
/**
 * MainAbility
 *
 * @since 2021-04-26
 **/
@Router(value = {"http://mzule.com/main", "main", "home"},
        longParams = {"id", "updateTime"},
        booleanParams = "web",
        transfer = "web=>fromWeb")
public class MainAbility extends DumpExtrasAbility {
    /**
     * 定义一个 用于接收的变量
     */
    public static final int RESULT_OK = -1;

    @Override
    protected void onActive() {
        super.onActive();
        Intent resultIntent = new Intent();
        setResult(RESULT_OK,resultIntent);
    }

}
