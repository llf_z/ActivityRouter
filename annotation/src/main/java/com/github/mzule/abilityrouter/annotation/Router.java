package com.github.mzule.abilityrouter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Router
 *
 * @since 2021-04-16
 * */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.CLASS)
public @interface Router {
    /**
     * 类型
     *
     * @return value 类型
     * */
    String[] value();
    /**
     * 字符串参数
     *
     * @return stringParams 字符串
     * */
    String[] stringParams() default "";
    /**
     * 整形参数
     *
     * @return intParams 整形
     * */
    String[] intParams() default "";
    /**
     * 长参数
     *
     * @return longParams long类型
     * */
    String[] longParams() default "";
    /**
     * boolean类型参数
     *
     * @return booleanParams 布尔
     * */
    String[] booleanParams() default "";
    /**
     * short类型参数
     *
     * @return shortParams short类型
     * */
    String[] shortParams() default "";
    /**
     * float类型参数
     *
     * @return floatParams float类型
     * */
    String[] floatParams() default "";
    /**
     * double类型参数
     *
     * @return doubleParams double类型
     * */
    String[] doubleParams() default "";
    /**
     * byte类型参数
     *
     * @return byteParams byte类型
     * */
    String[] byteParams() default "";
    /**
     * char类型参数
     *
     * @return charParams char类型
     * */
    String[] charParams() default "";
    /**
     * 转移
     *
     * @return transfer 转移
     * */
    String[] transfer() default "";
}
